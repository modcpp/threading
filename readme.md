# Threading

## TODOs

    1) std::shared_mutex instead of std::mutex (should improve performance)
    2) std::atomic variable instead of non atomic + mutex; needs an atomic implementation for substract_if_less_than (should be the fastest implementation)
    3) add an example with assync