#include "Atm.h"

#include "Logger.h"

#include <thread>

using namespace std::chrono_literals;

basic::Atm::Atm() :
	m_balance(),
	m_processingTime(1000ms)
{
	// Empty
}

void basic::Atm::Deposit(const AmountType& value, const std::string& userName)
{
	logi("User:", userName, "attempting to deposit:", value);

	std::lock_guard<std::mutex> lockBalance(m_mutexBalance);
	std::this_thread::sleep_for(m_processingTime);
	logi("User:", userName, "depositing:", value);	
	m_balance += value;
}

bool basic::Atm::Withdraw(const AmountType& value, const std::string& userName)
{
	logi("User:", userName, "attempting to withdraw:", value);
	
	{
		std::lock_guard<std::mutex> lockBalance(m_mutexBalance);
		std::this_thread::sleep_for(m_processingTime / 2);
		if (m_balance < value)
		{
			logi("User:", userName, "failed to withdraw; reason: not enough money");
			return false;
		}

		logi("User:", userName, "withdrawing:", value);

		std::this_thread::sleep_for(m_processingTime);
		m_balance -= value;
	}

	return true;
}

const basic::Atm::AmountType& basic::Atm::GetBalance() const
{
	std::lock_guard<std::mutex> lockBalance(m_mutexBalance);
	std::this_thread::sleep_for(m_processingTime/4);
	return m_balance;
}
