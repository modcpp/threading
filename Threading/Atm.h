#pragma once
#include <cstdint>
#include <string>
#include <chrono>
#include <mutex>

namespace basic
{
	class Atm
	{
	public:
		using AmountType = int32_t;

	public:
		Atm();
		void Deposit(const AmountType& value, const std::string& userName);
		bool Withdraw(const AmountType& value, const std::string& userName);

		const AmountType& GetBalance() const;

	private:
		AmountType m_balance;
		std::chrono::milliseconds m_processingTime;
		mutable std::mutex m_mutexBalance;
	};
}