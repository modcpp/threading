#include "Atm.h"

#include "Logger.h"

#include <thread>
#include <vector>
#include <atomic>

using namespace basic;

void sequentialWithdrawal()
{
	logi("#######", __FUNCTION__, "#######");

	Atm atm;
	atm.Deposit(1000, "Bank");

	atm.Withdraw(500, "John");
	atm.Withdraw(300, "Emma");
	atm.Withdraw(400, "Nicholas");

	logi("Final Atm balance:", atm.GetBalance());
}

void threadedWithdrawal()
{
	logi("#######", __FUNCTION__, "#######");

	Atm atm;
	atm.Deposit(500, "Bank");

	// create tasks
	auto johnsTask = [] (Atm& atm) { // pass atm as a parameter of the function, see below how atm is passed
		atm.Withdraw(500, "John");
	};
	auto emmasTask = [&atm] () {
		atm.Withdraw(300, "Emma");
	};
	auto nicholassTask = [&atm] () {
		atm.Withdraw(400, "Nicholas");
	};

	// lunch tasks and retain threads in a vector
	std::vector<std::thread> workers;
	workers.emplace_back([&atm]() {
		// bank deposits 5 times
		for(uint32_t i = 0; i < 5; ++i)
			atm.Deposit(200, "Bank");
	});	// create thread with inline lambda
	std::thread workerJohn(johnsTask, std::ref(atm));	// pass atm as a parameter of the function
	workers.push_back(std::move(workerJohn));	// can only move threads (not copy)
	workers.emplace_back(std::thread(emmasTask));	// temporary thread moved to vector
	workers.emplace_back(nicholassTask);	// thread created inside of vector with the necessary parameter

	std::atomic_bool watchBalance = true;
	std::thread balanceWatcher([&atm, &watchBalance]() {
		while (watchBalance)
		{
			logi("Current Atm balance:", atm.GetBalance());
			//using namespace std::chrono_literals;
			//std::this_thread::sleep_for(500ms);
		}
	});

	// wait for workers to finish their tasks
	for (auto& worker : workers)
		worker.join();

	// stop balanceWatcher and wait for it to finish
	watchBalance = false;
	balanceWatcher.join();

	logi("Final Atm balance:", atm.GetBalance());
}

int main()
{
	sequentialWithdrawal();
	threadedWithdrawal();
	
	return 0;
}